/* FortuneAlgorithm
 * Copyright (C) 2018 Pierre Vigier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// STL
#include <iostream>
#include <vector>
#include <chrono>
#include <random>
// SFML
#include <SFML/Graphics.hpp>
// My includes
#include "voronoi.h"

Voronoi* vdg;
vector<VoronoiPoint*> ver;
vector<VEdge> edges;

constexpr float WINDOW_WIDTH = 600.0f;
constexpr float WINDOW_HEIGHT = 600.0f;

void display(sf::RenderWindow& win){
	for (vector<VoronoiPoint*>::iterator i = ver.begin(); i != ver.end(); i++){
		sf::Vertex point(sf::Vector2f((*i)->x, (*i)->y), sf::Color::Red);
        win.draw(&point, 1, sf::Points);
	}


	for (vector<VEdge>::iterator j = edges.begin(); j != edges.end(); j++)
	{
	    sf::Vertex line[] =
        {
            sf::Vertex(sf::Vector2f(j->VertexA.x, j->VertexA.y)),
            sf::Vertex(sf::Vector2f(j->VertexB.x, j->VertexB.y))
        };
        win.draw(line, 2, sf::Lines);
	}
}

int main()
{
	int iou = 0;
	for (vector<VoronoiPoint*>::iterator i = ver.begin(); i != ver.end(); i++)
		delete((*i));
	ver.clear();
	edges.clear();
    cout << ((double) rand() / (RAND_MAX)) + 1;
	for (int i = 0; i <= 60; i++){
        double x = (((double) rand() / (RAND_MAX)) - 0.5) * 4;
        double y = (((double) rand() / (RAND_MAX)) - 0.5) * 4;
        ver.push_back(new VoronoiPoint(x, y));
	}

	vdg = new Voronoi();
	double minY = -10;
	double maxY = 10;
	edges = vdg->ComputeVoronoiGraph(ver, minY, maxY);
	delete vdg;


    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;
    sf::RenderWindow window(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Fortune's algorithm", sf::Style::Default, settings);
    window.setView(sf::View(sf::FloatRect(-0.1f, -0.1f, 2.0f, 2.0f)));

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            else if (event.type == sf::Event::KeyReleased && event.key.code == sf::Keyboard::Key::N)
                cout << "";
        };

        display(window);
        window.display();
    }

    return 0;
}
